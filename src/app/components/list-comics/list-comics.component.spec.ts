import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListComicsComponent } from './list-comics.component';
import { HttpClientModule } from '@angular/common/http';

describe('ListComicsComponent', () => {
  let component: ListComicsComponent;
  let fixture: ComponentFixture<ListComicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListComicsComponent],
      imports: [HttpClientModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have "My favourites" title', () => {
    const bannerElement: HTMLElement = fixture.nativeElement;
    const h3 = bannerElement.querySelector('h3');
    expect(h3.textContent).toContain('My favourites');
  });

  it('should have some comic', (done) => {
    component.heroId = 1017100;
    component.loadList();
    setTimeout(() => {
      expect(component.comics.length).toBeGreaterThan(0);
      done();
    }, 4000);
  });
});
