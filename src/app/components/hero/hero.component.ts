import { Component, Input, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { ListComicsComponent } from '../list-comics/list-comics.component';
@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.sass']
})
export class HeroComponent implements OnInit {
  
  @Input('hero') hero: any = {};

  constructor(
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
  }
  
  public get imageSrc() : string {
    return this.hero.thumbnail.path + '/standard_amazing.' + this.hero.thumbnail.extension
  }
  
  openComics(){
    const modalRef = this.modalService.open(ListComicsComponent);
    modalRef.componentInstance.heroId = this.hero.id
  }

}
