import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeroComponent } from './hero/hero.component';
import { SearchbarComponent } from './searchbar/searchbar.component';
import { ContentComponent } from './content/content.component';
import { ListComicsComponent } from './list-comics/list-comics.component';
import { ComicComponent } from './comic/comic.component';
import { FormsModule } from '@angular/forms';
import { LoadingComponent } from './loading/loading.component';
import { PaginationComponent } from './pagination/pagination.component';



@NgModule({
  declarations: [HeroComponent, SearchbarComponent, ContentComponent, ListComicsComponent, ComicComponent, LoadingComponent, PaginationComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [ SearchbarComponent, ContentComponent, ListComicsComponent]
})
export class ComponentsModule { }
