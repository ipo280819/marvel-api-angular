import { TestBed } from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} 
       from '@angular/common/http/testing';

import { MarvelService } from './marvel.service';
import { HttpClientModule } from '@angular/common/http';

describe('MarvelService', () => {
  let service: MarvelService;
  const name = 'spid'

  let originalTimeout;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MarvelService],
      imports: [HttpClientModule]
    });

    service = TestBed.inject(MarvelService);

    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 8000;
  });

  afterEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#getHerosPage should return 10 results', (done: DoneFn)=>{
    service.getHerosStartWith().subscribe( value => {
      expect(value.data.results.length).toBe(10);
      done();
    }, err => {
      fail('Fail Request')
      done();
    })
  });
  it(`#getHerosStartWith should return 10 results and start with ${name}`, (done: DoneFn)=>{
    service.getHerosStartWith(1,name).subscribe( value => {
      
      expect(value.data.results.filter(value => value.name.toUpperCase().startsWith(name.toUpperCase()) ).length).toBe(10);
      done();
    }, err => {
      fail('Fail Request')
      done();
    })
  });

  it('#getHerosStartWith should return 0 results', (done: DoneFn)=>{
    service.getHerosStartWith(1,'xxxxxxxx').subscribe( value => {
      
      expect(value.data.results.length).toBe(0);
      done();
    }, err => {
      fail('Fail Request')
      done();
    })
  });
});
