import { Component, OnInit } from '@angular/core';
import { MarvelService } from '../../services/marvel.service';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.sass']
})
export class SearchbarComponent implements OnInit {

  public name = '';
  constructor(
    private marvelService: MarvelService
  ) { }

  ngOnInit(): void {

  }
  onSubmit(){
    this.marvelService.emitChangeFilter(this.name)
  }
}
